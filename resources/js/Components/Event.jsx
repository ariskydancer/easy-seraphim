

export default function Event({ event }) {
    return (
        <div className="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
            <div className="p-6 text-gray-900 dark:text-gray-100">
                <h2 className="text-2xl"><a rel="noopener" target="window" href={event.link}>{event.title}</a></h2>
                <p className="text-gray-500">Opens {event.event_opening_date_formatted}</p>
                <p className="text-gray-500">Closes {event.event_closing_date_formatted}</p>
            </div>
        </div>
    );
}
