<?php

namespace App\Console\Commands;

use App\Helpers\Feed;
use Illuminate\Console\Command;

class FetchFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:fetch-feed';

    /**
     * The feed
     *
     * @var string
     */
    protected string $feed = 'https://www.seraphimsl.com/rss';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        // Fetch the feed and parse it
        $feed = new Feed($this->feed);
        $items = $feed->fetch();

        // Loop through the items and insert them into the database if they don't already exist
        foreach ($items['entries'] as $item) {
            $event = \App\Models\Event::where('title', $item['title'])
                ->where('event_opening_date', $item['event_opening_date'])
                ->first();

            if (!$event) {
                \App\Models\Event::create($item);
                echo "Added {$item['title']} to the database\n";
            }
        }

    }
}
