<?php

namespace App\Helpers;


use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use Laminas\Feed\Reader\Reader;

/**
 * The class fetches and parses a feed provided through the constructor
 */
class Feed
{
    /**
     * The feed
     *
     * @var string
     */
    protected string $feed;

    /**
     * The constructor
     *
     * @param string $feed
     */
    public function __construct(string $feed)
    {
        $this->feed = $feed;
    }

    /**
     * Fetches and parses the feed
     *
     * @return array
     * @throws GuzzleException
     */
    public function fetch(): array
    {
        Reader::setHttpClient(new GuzzleClient());

        $feed = Reader::import($this->feed);

        $data = [
            'title'        => $feed->getTitle(),
            'link'         => $feed->getLink(),
            'dateModified' => $feed->getDateModified(),
            'description'  => $feed->getDescription(),
            'language'     => $feed->getLanguage(),
            'entries'      => [],
        ];

        foreach ($feed as $entry) {
            $openingDate = null;
            $closingDate = null;

            // Parse content for the event opening and closing dates
            $content = $entry->getContent();
            $content = strip_tags($content);
            // Locate the opening date that follows the words "Event Opening Date:" and ends with a year
            $found = preg_match('/Event Opening Date:.*\d{4}/', $content, $matches);
            // If the opening date is found, then store it in the $openingDate variable
            if ($found) {
                // remove the words "Event Opening Date:" from the string
                $date = str_replace('Event Opening Date: ', '', $matches[0]);
                $openingDate = Carbon::parse($date);
            }

            // Locate the closing date that follows the words "Event Closing Date:" and ends with a year
            $found = preg_match('/Event Closing Date:.*\d{4}/', $content, $matches);
            // If the closing date is found, then store it in the $closingDate variable as a Carbon object
            if ($found) {
                // remove the words "Event Closing Date:" from the string
                $date = str_replace('Event Closing Date: ', '', $matches[0]);
                $closingDate = Carbon::parse($date);
            }

            $authors = (array)$entry->getAuthors();
            $authors = array_map(function ($author) {
                return $author['name'];
            }, $authors);

            $edata = [
                'title'                 => $entry->getTitle(),
                'description'           => $entry->getDescription(),
                'date_modified'         => $entry->getDateModified(),
                'authors'               => implode(", ", $authors),
                'link'                  => $entry->getLink(),
                'content'               => $entry->getContent(),
                'event_opening_date'    => $openingDate?->startOfDay(),
                'event_closing_date'    => $closingDate?->endOfDay(),
            ];
            $data['entries'][] = $edata;
        }

        // Return the feed items
        return $data;
    }

}
