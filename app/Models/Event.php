<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
        'title',
        'link',
        'date_modified',
        'description',
        'authors',
        'content',
        'event_opening_date',
        'event_closing_date',
    ];

    /**
     * The attributes that should be cast.
     */
    protected $casts = [
        'date_modified' => 'datetime',
        'event_opening_date' => 'datetime',
        'event_closing_date' => 'datetime',
    ];

    /**
     * The attributes that should be mutated to dates.
     */
    protected $dates = [
        'date_modified',
        'event_opening_date',
        'event_closing_date',
    ];

    /**
     * The attributes that should be hidden for arrays.
     */
    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be appended to arrays.
     */
    protected $appends = [
        'event_opening_date_formatted',
        'event_closing_date_formatted',
    ];

    /**
     * Get the event opening date formatted.
     *
     * @return string
     */
    public function getEventOpeningDateFormattedAttribute()
    {
        return $this->event_opening_date->format('F j, Y');
    }

    /**
     * Get the event closing date formatted.
     *
     * @return string
     */
    public function getEventClosingDateFormattedAttribute()
    {
        return $this->event_closing_date->format('F j, Y');
    }
}
